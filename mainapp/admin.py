from django.contrib import admin
from mainapp.models import *


class PriceItems(admin.TabularInline):
    model = PriceItem
    extra = 4


class ProductsAdmin(admin.ModelAdmin):
    inlines = [PriceItems]
    list_display = ['title', 'slug', 'draft', 'popular_products']
    exclude = ('title', 'content')


admin.site.register(Products, ProductsAdmin)


class AboutusAdmin(admin.ModelAdmin):
    exclude = ('title', 'content', 'h_title', 'h_content')


admin.site.register(AboutUs, AboutusAdmin)


class SettingsAdmin(admin.ModelAdmin):
    exclude = ('adress',)


admin.site.register(Settings, SettingsAdmin)


class FeedbackAdmin(admin.ModelAdmin):
    exclude = ('name', 'feedback_content')


admin.site.register(Feedback, FeedbackAdmin)


class SliderAdmin(admin.ModelAdmin):
    list_display = ['text_1', 'draft']


admin.site.register(Slider, SliderAdmin)


class StatisticsAdmin(admin.ModelAdmin):
    list_display = ['name', 'value']
    exclude = ('name',)


admin.site.register(Statistics, StatisticsAdmin)


class FAQAdmin(admin.ModelAdmin):
    exclude = ('name', 'feedback_content')


admin.site.register(FAQ, FAQAdmin)

admin.site.register(Breadcrumbs)

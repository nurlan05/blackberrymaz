# Generated by Django 2.2.3 on 2022-04-05 05:25

import ckeditor.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AboutUs',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('meta_title', models.CharField(max_length=1500, null=True, verbose_name='Meta title')),
                ('meta_keywords', models.CharField(blank=True, max_length=1500, null=True, verbose_name='Meta keywords')),
                ('meta_description', models.TextField(help_text='Meta descriptionda max. 150 hərf ola bilər!', max_length=160, null=True, verbose_name='Meta description')),
                ('title', models.CharField(max_length=1500, verbose_name='Başlıq')),
                ('content', ckeditor.fields.RichTextField(verbose_name='Mətn')),
                ('image', models.FileField(upload_to='', verbose_name='Şəkil(602x468)')),
                ('slug', models.SlugField(editable=False, verbose_name='Slug')),
            ],
            options={
                'verbose_name': 'Haqqımızda',
                'verbose_name_plural': 'Haqqımızda',
            },
        ),
        migrations.CreateModel(
            name='Products',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=5000, verbose_name='Məhsulun adı')),
                ('image', models.ImageField(upload_to='', verbose_name='Məhsulun şəkli')),
                ('c_image', models.ImageField(blank=True, upload_to='', verbose_name='Məhsulun cover şəkli')),
                ('content', ckeditor.fields.RichTextField(verbose_name='Məhsul haqqında məlumat')),
                ('slug', models.SlugField(editable=False, null=True, verbose_name='Slug')),
            ],
            options={
                'verbose_name': 'Məhsul',
                'verbose_name_plural': 'Məhsullar',
            },
        ),
        migrations.CreateModel(
            name='Settings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('about_text', models.CharField(max_length=1500, verbose_name='Sayt haqqında(SEO mətni)')),
                ('meta_keywords', models.CharField(blank=True, max_length=1500, null=True, verbose_name='Meta Keywords')),
                ('site_title', models.CharField(max_length=1500, verbose_name='Saytın başlığı')),
                ('email', models.CharField(max_length=1500, null=True, verbose_name='Email')),
                ('number', models.CharField(max_length=1500, null=True, verbose_name='Nömrə')),
                ('adress', models.CharField(max_length=1500, null=True, verbose_name='Ünvan')),
                ('logo', models.FileField(blank=True, upload_to='', verbose_name='logo(163x38)')),
                ('favicon', models.FileField(blank=True, upload_to='', verbose_name='favicon(32x32)')),
                ('facebook', models.CharField(blank=True, max_length=1500, verbose_name='Facebook')),
                ('instagram', models.CharField(blank=True, max_length=1500, verbose_name='İnstagram')),
                ('linkedin', models.CharField(blank=True, max_length=1500, verbose_name='Linkedin')),
                ('youtube', models.CharField(blank=True, max_length=1500, verbose_name='Youtube')),
                ('medium', models.CharField(blank=True, max_length=1500, verbose_name='Medium')),
                ('google_business', models.CharField(blank=True, max_length=1500, null=True, verbose_name='Google Business')),
                ('twitter', models.CharField(blank=True, max_length=1500, null=True, verbose_name='Twitter')),
                ('slug', models.SlugField(editable=False, verbose_name='Slug')),
                ('c_meta_title', models.CharField(max_length=1500, null=True, verbose_name='Əlaqə Meta title')),
                ('c_meta_keywords', models.CharField(blank=True, max_length=1500, null=True, verbose_name='Əlaqə Meta keywords')),
                ('c_meta_description', models.TextField(help_text='Meta descriptionda max. 150 hərf ola bilər!', max_length=160, null=True, verbose_name='Əlaqə Meta description')),
                ('n_meta_title', models.CharField(max_length=1500, null=True, verbose_name='Xəbər Meta title')),
                ('n_meta_description', models.TextField(help_text='Meta descriptionda max. 150 hərf ola bilər!', max_length=160, null=True, verbose_name='Xəbər Meta description')),
            ],
            options={
                'verbose_name': 'Tənzimləmə',
                'verbose_name_plural': 'Tənzimləmələr',
                'ordering': ['-id'],
            },
        ),
        migrations.CreateModel(
            name='PriceItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('size', models.CharField(max_length=255, verbose_name='Məhsulun ölçüsü')),
                ('price', models.CharField(max_length=255, verbose_name='Məhsulun qiyməti')),
                ('resident', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='prices', to='mainapp.Products')),
            ],
        ),
    ]

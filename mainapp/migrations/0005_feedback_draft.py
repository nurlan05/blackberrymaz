# Generated by Django 2.2.3 on 2022-04-05 06:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0004_products_popular_products'),
    ]

    operations = [
        migrations.AddField(
            model_name='feedback',
            name='draft',
            field=models.BooleanField(default=True, verbose_name='Saytda yayımlansın?'),
        ),
    ]

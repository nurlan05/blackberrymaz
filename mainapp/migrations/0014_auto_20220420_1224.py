# Generated by Django 2.2.3 on 2022-04-20 08:24

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0013_auto_20220420_1223'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='aboutus',
            name='content_az',
        ),
        migrations.RemoveField(
            model_name='aboutus',
            name='content_en',
        ),
        migrations.RemoveField(
            model_name='aboutus',
            name='content_ru',
        ),
        migrations.RemoveField(
            model_name='aboutus',
            name='title_az',
        ),
        migrations.RemoveField(
            model_name='aboutus',
            name='title_en',
        ),
        migrations.RemoveField(
            model_name='aboutus',
            name='title_ru',
        ),
    ]

# Generated by Django 2.2.3 on 2022-04-20 09:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainapp', '0016_auto_20220420_1229'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='aboutus',
            name='content_az',
        ),
        migrations.RemoveField(
            model_name='aboutus',
            name='content_en',
        ),
        migrations.RemoveField(
            model_name='aboutus',
            name='content_ru',
        ),
        migrations.RemoveField(
            model_name='aboutus',
            name='h_content_az',
        ),
        migrations.RemoveField(
            model_name='aboutus',
            name='h_content_en',
        ),
        migrations.RemoveField(
            model_name='aboutus',
            name='h_content_ru',
        ),
        migrations.RemoveField(
            model_name='aboutus',
            name='h_title_az',
        ),
        migrations.RemoveField(
            model_name='aboutus',
            name='h_title_en',
        ),
        migrations.RemoveField(
            model_name='aboutus',
            name='h_title_ru',
        ),
        migrations.RemoveField(
            model_name='aboutus',
            name='title_az',
        ),
        migrations.RemoveField(
            model_name='aboutus',
            name='title_en',
        ),
        migrations.RemoveField(
            model_name='aboutus',
            name='title_ru',
        ),
        migrations.AlterField(
            model_name='aboutus',
            name='meta_description',
            field=models.TextField(help_text='Meta descriptionda max. 150 hərf ola bilər!', max_length=160, null=True, verbose_name='Meta description'),
        ),
    ]

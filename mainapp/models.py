from ckeditor.fields import RichTextField
from django.conf import settings
from django.db import models
from django.urls import reverse
from accounts.helper import slugify


class AboutUs(models.Model):
    meta_title = models.CharField(max_length=1500, verbose_name="Meta title", null=True, blank=False)
    meta_keywords = models.CharField(max_length=1500, verbose_name="Meta keywords", null=True, blank=True)
    meta_description = models.TextField(max_length=160, verbose_name="Meta description", null=True, blank=False,
                                        help_text="Meta descriptionda max. 150 hərf ola bilər!")
    title = models.CharField(max_length=1500, verbose_name="Başlıq")
    content = RichTextField(verbose_name="Mətn")
    image = models.FileField(verbose_name="Şəkil(602x468)")
    slug = models.SlugField(editable=False, verbose_name="Slug")
    h_title = models.CharField(max_length=1500, verbose_name="Ana səhifədə başlıq", null=True)
    h_content = RichTextField(verbose_name="Ana səhifədə mətn", null=True)

    def __str__(self):
        return ('%s') % (self.title)

    class Meta:
        verbose_name = "Haqqımızda"
        verbose_name_plural = "Haqqımızda"

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(AboutUs, self).save(*args, **kwargs)


class Settings(models.Model):
    about_text = models.CharField(max_length=1500, verbose_name="Sayt haqqında(SEO mətni)")
    meta_keywords = models.CharField(max_length=1500, verbose_name="Meta Keywords", null=True, blank=True)
    site_title = models.CharField(max_length=1500, verbose_name="Saytın başlığı")
    email = models.CharField(max_length=1500, verbose_name="Email", null=True)
    number = models.CharField(max_length=1500, verbose_name="Nömrə", null=True)
    adress = models.CharField(max_length=1500, verbose_name="Ünvan", null=True)
    g_adress = models.CharField(max_length=1500, verbose_name="Google Map linki", null=True,blank=True)
    logo = models.FileField(verbose_name="Qara logo(163x38)", blank=True)
    dark_logo = models.FileField(verbose_name="Ağ logo(163x38)", blank=True, null=True)
    favicon = models.FileField(verbose_name="favicon(32x32)", blank=True, null=True)

    proloader_logo = models.FileField(verbose_name="Preloader logo(163x38)",help_text="Sayt açılanda yükənən logo", blank=True, null=True)
    footer_logo = models.FileField(verbose_name="Footer logo(163x38)",help_text="Saytın aşağısındakı logo", blank=True, null=True)
    facebook = models.CharField(max_length=1500, verbose_name="Facebook", blank=True)
    instagram = models.CharField(max_length=1500, verbose_name="İnstagram", blank=True)
    linkedin = models.CharField(max_length=1500, verbose_name="Linkedin", blank=True)
    youtube = models.CharField(max_length=1500, verbose_name="Youtube", blank=True)
    medium = models.CharField(max_length=1500, verbose_name="Medium", blank=True)
    google_business = models.CharField(max_length=1500, verbose_name="Google Business", blank=True, null=True)
    twitter = models.CharField(max_length=1500, verbose_name="Twitter", null=True, blank=True)
    slug = models.SlugField(editable=False, verbose_name="Slug")
    c_meta_title = models.CharField(max_length=1500, verbose_name="Əlaqə Meta title", null=True, blank=False)
    c_meta_keywords = models.CharField(max_length=1500, verbose_name="Əlaqə Meta keywords", null=True, blank=True)
    c_meta_description = models.TextField(max_length=160, verbose_name="Əlaqə Meta description", null=True,
                                          blank=False, help_text="Meta descriptionda max. 150 hərf ola bilər!")
    n_meta_title = models.CharField(max_length=1500, verbose_name="Xəbər Meta title", null=True, blank=False)
    n_meta_description = models.TextField(max_length=160, verbose_name="Xəbər Meta description", null=True,
                                          blank=False, help_text="Meta descriptionda max. 150 hərf ola bilər!")

    def __str__(self):
        return ('%s') % (self.site_title)

    class Meta:
        verbose_name = "Tənzimləmə"
        verbose_name_plural = "Tənzimləmələr"
        ordering = ['-id']

    def save(self, *args, **kwargs):
        self.slug = slugify(self.site_title)
        super(Settings, self).save(*args, **kwargs)


class Products(models.Model):
    title = models.CharField(max_length=5000, verbose_name="Məhsulun adı", blank=False)
    image = models.ImageField(verbose_name="Məhsulun şəkli", blank=False)
    f_image = models.ImageField(verbose_name="Məhsulun icon şəkli", blank=False, null=True)
    c_image = models.ImageField(verbose_name="Məhsulun cover şəkli(Komp ucun)", blank=True)
    c_image_m = models.ImageField(verbose_name="Məhsulun cover şəkli(Mobile üçün ucun)", blank=True, null=True)
    content = RichTextField(verbose_name="Məhsul haqqında məlumat", blank=False)
    popular_products = models.BooleanField(default=False, verbose_name="Ana səhifədə yayımlansın?")
    draft = models.BooleanField(default=True, verbose_name="Saytda yayımlansın?")
    slug = models.SlugField(editable=False, verbose_name="Slug", null=True)

    def __str__(self):
        return ('%s') % (self.title)

    class Meta:
        verbose_name = "Məhsul"
        verbose_name_plural = "Məhsullar"

    def get_absolute_url(self):
        return reverse('mainapp:product_detail_view', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        super(Products, self).save(*args, **kwargs)
        self.slug = "{}".format(slugify(self.title))
        super(Products, self).save(*args, **kwargs)


class PriceItem(models.Model):
    resident = models.ForeignKey(Products,
                                 on_delete=models.CASCADE,
                                 related_name="prices")
    size = models.CharField(max_length=255, verbose_name="Məhsulun ölçüsü")
    price = models.CharField(max_length=255, verbose_name="Məhsulun qiyməti")

    def __str__(self):
        return ('%s') % (self.size)


class Slider(models.Model):
    text_1 = models.CharField(max_length=1200, verbose_name="Kiçik yazı", blank=True)
    text_2 = models.CharField(max_length=1200, verbose_name="Böyük yazı", blank=True)
    button_text = models.CharField(max_length=1200, verbose_name="Butonun mətni", blank=True)
    link = models.CharField(max_length=1200, verbose_name="Link", blank=True)
    image = models.FileField(verbose_name="Şəkil")
    draft = models.BooleanField(default=True, verbose_name="Saytda yayımlansın?")

    def __str__(self):
        return ('%s') % (str("Slider") + " " + str("#") + str(self.id))

    class Meta:
        verbose_name = "Slayd"
        verbose_name_plural = "Slaydlar"
        ordering = ['-id']


class Feedback(models.Model):
    name = models.CharField(max_length=1500, verbose_name="Müştərinin adı")
    feedback_content = RichTextField(verbose_name="Feedback mətni")
    is_about_view = models.BooleanField(default=False, verbose_name="Haqqımızda səhifəsində göstərilsin?")

    draft = models.BooleanField(default=True, verbose_name="Saytda yayımlansın?")
    slug = models.SlugField(editable=False, verbose_name="Slug")

    def __str__(self):
        return ('%s') % (self.name)

    class Meta:
        verbose_name = "Feedback"
        verbose_name_plural = "Feedbacklər"
        ordering = ['-id']

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Feedback, self).save(*args, **kwargs)


class Statistics(models.Model):
    name = models.CharField(max_length=1500, verbose_name="Göstəricinin adı")
    value = models.CharField(max_length=1500, verbose_name="Göstəricinin dəyəri")
    draft = models.BooleanField(default=True, verbose_name="Saytda yayımlansın?")
    slug = models.SlugField(editable=False, verbose_name="Slug")

    def __str__(self):
        return ('%s') % (self.name)

    class Meta:
        verbose_name = "Statistika"
        verbose_name_plural = "Statistikalar"
        ordering = ['-id']

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super(Statistics, self).save(*args, **kwargs)


class FAQ(models.Model):
    name = models.CharField(max_length=1500, verbose_name="Sual")
    feedback_content = RichTextField(verbose_name="Cavab")
    draft = models.BooleanField(default=True, verbose_name="Saytda yayımlansın?")

    id_collapse= models.CharField(max_length=1500,verbose_name="Sıralama collapse",null=True,blank=True,help_text="Buna toxunmayın")
    id_heading = models.CharField(max_length=1500, verbose_name="Sıralama headıng",null=True,blank=True,help_text="Buna toxunmayın")
    slug = models.SlugField(editable=False, verbose_name="Slug")

    def __str__(self):
        return ('%s') % (self.name)

    class Meta:
        verbose_name = "Sual"
        verbose_name_plural = "Suallar"
        ordering = ['id']

    def save(self, *args, **kwargs):
        super(FAQ, self).save(*args, **kwargs)
        if self.id == 1:
            self.id_collapse = "{}".format("CollapseOne")
            self.id_heading = "{}".format("HeadingOne")
        if self.id == 2:
            self.id_collapse = "{}".format("CollapseTwo")
            self.id_heading = "{}".format("HeadingTwo")
        if self.id == 3:
            self.id_collapse = "{}".format("CollapseThree")
            self.id_heading = "{}".format("HeadingThree")
        if self.id == 4:
            self.id_collapse = "{}".format("CollapseFour")
            self.id_heading = "{}".format("HeadingFour")
        super(FAQ, self).save(*args, **kwargs)


class Breadcrumbs(models.Model):
    title = models.CharField(max_length=1500, verbose_name="Başlıq")
    a_image_l = models.FileField(verbose_name="Ana səhifə haqqımızda(sol)")
    a_image_r = models.FileField(verbose_name="Ana səhifə haqqımızda(sağ)")
    f_image_r = models.FileField(verbose_name="Ana səhifə tez-tez verilen sullar(sağ)")
    comment_image = models.FileField(verbose_name="Ana səhifə şərhlər (arxa)")
    pro_cover_image = models.FileField(verbose_name="Məhsullarımız cover şəkli(ən yuxarı)")
    about_cover_image = models.FileField(verbose_name="Haqqımızda cover şəkli(ən yuxarı)")
    contact_cover_image = models.FileField(verbose_name="Bizimlə əlaqə cover şəkli(ən yuxarı)")

    def __str__(self):
        return ('%s') % (self.title)

    class Meta:
        verbose_name = "Şəkil"
        verbose_name_plural = "Saytdakı bəzi şəkillər"

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Breadcrumbs, self).save(*args, **kwargs)

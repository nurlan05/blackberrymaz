import threading
from django.core.mail import send_mail
from django.conf import settings

def email_send_task(*args):
    subject, message, name, email = args

    message = """
        Sender : {}
        Content: {}
        Name : {}
    """.format(
        email,
        message,
        name,
    )

    send_mail(
        subject,
        message,
        settings.EMAIL_HOST_USER,
        [settings.EMAIL_HOST_USER],
        fail_silently=False
    )
    return "Success"



def send_mail_task(*args):
    background = threading.Thread(target=email_send_task, args=args)
    background.start()
    return "Send"
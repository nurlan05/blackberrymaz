from modeltranslation.translator import translator, TranslationOptions
from .models import *


class AboutUsOptions(TranslationOptions):
    fields = ('title', 'content', 'h_title', 'h_content')


translator.register(AboutUs, AboutUsOptions)


class SettingsOptions(TranslationOptions):
    fields = ('adress',)


translator.register(Settings, SettingsOptions)


class ProductsOptions(TranslationOptions):
    fields = ('title', 'content')


translator.register(Products, ProductsOptions)


class SliderOptions(TranslationOptions):
    fields = ('text_1', 'text_2', 'button_text',)


translator.register(Slider, SliderOptions)


class FeedbackOptions(TranslationOptions):
    fields = ('name', 'feedback_content')


translator.register(Feedback, FeedbackOptions)


class StatisticsOptions(TranslationOptions):
    fields = ('name',)


translator.register(Statistics, StatisticsOptions)


class FAQOptions(TranslationOptions):
    fields = ('name', 'feedback_content',)


translator.register(FAQ,FAQOptions)

from django.urls import path
from . import views

app_name = 'mainapp'

urlpatterns = [
    path('',views.index,name='index'),
    path('about/',views.about_view,name='about'),
    path('contact/',views.contact_view,name='contact'),
    path('products/',views.product_list_view,name='products'),
    path('product/<slug>/',views.product_detail_view,name='product_detail_view'),
]
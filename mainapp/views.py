from django.shortcuts import render
from mainapp.models import *
from django.contrib import messages
from .sendmail import send_mail_task

# Create your views here.
def index(request):
    context = {}
    context['slider'] = Slider.objects.filter(draft=True).order_by('-id')
    context['popular_products'] = Products.objects.filter(draft=True, popular_products=True).order_by('-id')[:3]
    context['home_about'] = AboutUs.objects.all()[:1]
    context['statistics'] = Statistics.objects.filter(draft=True).order_by('id')[:3]
    context['faq'] = FAQ.objects.filter(draft=True).order_by('id')[:4]
    context['feedback'] = Feedback.objects.filter(draft=True).order_by('-id')
    context['settings'] = Settings.objects.all().order_by('-id')[:1]
    context['breadcrumbs'] = Breadcrumbs.objects.all()[:1]

    context['last_products'] = Products.objects.filter(draft=True).order_by('-id')[:2]
    return render(request, 'home/home.html', context)


# product view
def product_list_view(request):
    context = {}

    context['product_list'] = Products.objects.filter(draft=True).order_by('-id')

    context['settings'] = Settings.objects.all().order_by('-id')[:1]
    context['breadcrumbs'] = Breadcrumbs.objects.all()[:1]
    return render(request, 'products/products.html', context)


# =========== Product ==============================
def product_detail_view(request, slug):
    context = {}

    product = Products.objects.get(slug=slug)
    context['obj'] = product
    context['price_obj'] = PriceItem.objects.filter(resident=product).order_by('id')
    context['popular_products'] = Products.objects.filter(draft=True).exclude(pk=product.pk).order_by('-id')[:3]
    context['breadcrumbs'] = Breadcrumbs.objects.all()[:1]

    context['settings'] = Settings.objects.all().order_by('-id')[:1]
    return render(request, 'products/product_detail.html', context)


def about_view(request):
    context = {}
    context['about'] = AboutUs.objects.all()[:1]
    context['settings'] = Settings.objects.all().order_by('-id')[:1]
    context['breadcrumbs'] = Breadcrumbs.objects.all()[:1]

    return render(request, 'about/about.html', context)


def contact_view(request):
    context = {}

    if request.method == "POST":
        data = request.POST
        subject, message, name, email ,phone,= data.get('subject'), data.get('content'), data.get('name'), data.get(
            'email'),data.get('phone'),
        send_mail_task(subject, message, name, email,phone)
        messages.success(request, ('Mesajınız uğurla göndərildi'))
    context['breadcrumbs'] = Breadcrumbs.objects.all()[:1]

    context['settings'] = Settings.objects.all().order_by('-id')[:1]
    return render(request, 'connect/connect.html', context)
